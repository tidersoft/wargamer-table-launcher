﻿using System.Collections;
using System.Collections.Generic;
using TouchScript;
using TouchScript.Gestures;
using TouchScript.Hit;
using UnityEngine;

public class Toggle_one_screen : MonoBehaviour {

    private TapGesture gesture;

   
    private void OnEnable()
    {
      
        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;

    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }


    private void tappedHandler(object sender, System.EventArgs e)
    {
        var gesture = sender as TapGesture;
        TouchHit hit;
        gesture.GetTargetHitResult(out hit);

        GameObject.FindGameObjectWithTag("Engine").SendMessage("Toggle_one_screen");
    }
}
