﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class set_Language : MonoBehaviour {


    public Transform Polski;
    public Transform English;
	// Use this for initialization
	void Start () {
        if (Application.systemLanguage.Equals(SystemLanguage.Polish))
        {
            Polski.gameObject.SetActive(true);
            English.gameObject.SetActive(false);

        }
        else
        {

            Polski.gameObject.SetActive(false);
            English.gameObject.SetActive(true);

        }
    }
	
	
}
