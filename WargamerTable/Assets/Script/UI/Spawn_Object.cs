﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using TouchScript.Hit;
using UnityEngine;
using UnityEngine.Networking;

public class Spawn_Object : MonoBehaviour {

    private TapGesture gesture;
    public string board;
    public float Z;
    public bool isBoard;
  
    private void OnEnable()
    {

        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }

  
    private void tappedHandler(object sender, System.EventArgs e)
    {
        var gesture = sender as TapGesture;
        TouchHit hit;
        gesture.GetTargetHitResult(out hit);
        SpawnPoint sp=new SpawnPoint(board, new Vector3(hit.Point.x, Z, hit.Point.z));
        sp.board = isBoard;
        string josn = JsonUtility.ToJson(sp);
        GameObject.Find("System_Engine").BroadcastMessage("Spawn", josn, SendMessageOptions.RequireReceiver);

     
        //cube.RotateAround (hit.Point, new Vector3 (1, 0, 0), 90);
        //cube.GetComponent<Destroy_Menu> ().PopParent = transform.root.transform;
        transform.root.GetComponent<Destroy_Menu>().destroy();

    }
}
