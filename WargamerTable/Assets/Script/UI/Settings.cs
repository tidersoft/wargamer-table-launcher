﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {

    public Slider slider;

    public 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateSize(int i) {
        slider.setMax(i);
    }

    public void CurrentUpdate(int i)
    {
        slider.UpdateValue(i);
    }
}
