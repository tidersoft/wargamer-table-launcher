﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class Button_Next_Page : MonoBehaviour {


    public GameObject next_page;
    public GameObject this_page;
    private TapGesture gesture;

    private void OnEnable()
    {

        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }

    private void tappedHandler(object sender, System.EventArgs e)
    {

        this_page.SetActive(false);
        next_page.SetActive(true);

    }
}
