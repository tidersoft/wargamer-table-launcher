﻿using UnityEngine;
using System.Collections;
using TouchScript.Hit;
using TouchScript.Gestures;

public class CloseTutorial : MonoBehaviour {

	private TapGesture gesture;
	private Canvas canvas;

	private void OnEnable()
	{
		canvas = GameObject.Find ("Canvas").GetComponent<Canvas> ();

		gesture = GetComponent<TapGesture>();
		gesture.Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		gesture = GetComponent<TapGesture>();
		gesture.Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, System.EventArgs e)
	{
		if (canvas.enabled == true)
			canvas.enabled = false;
		 else 
			canvas.enabled = true;
				
		transform.RotateAround (transform.position, new Vector3 (0, 1, 0), 180);

	}
}
