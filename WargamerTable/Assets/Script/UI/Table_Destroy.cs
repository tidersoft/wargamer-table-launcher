﻿using UnityEngine;
using System.Collections;

public class Table_Destroy : MonoBehaviour {



	void OnCollisionEnter(Collision col){
	
		if (col.gameObject.transform.tag.Equals ("Token") || col.gameObject.transform.tag.Equals ("TokenElement")) {
			GameObject obj = Util.Tokens.getTokenRoot (col.gameObject.transform).gameObject;

			if (obj.tag.Equals ("Token")) {
				Destroy (obj);
			}
		}

	
	}
}
