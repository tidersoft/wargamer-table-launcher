﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progress_bar : MonoBehaviour {


    private int max_size,current_size;
    private float current_perc;

    public Transform Bar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CurrentUpdate(int current) {
        
        current_perc = (float)current / (float)max_size;
        Bar.Find("Progres").transform.localScale.Set(current_perc,1,1);
        Bar.Find("Progres").transform.localPosition.Set(-0.5f+(current_perc*0.05f),0,0);
    }

    public void UpdateSize(int size) {
        
        max_size = size;
        CurrentUpdate(0);
       
    }
}
