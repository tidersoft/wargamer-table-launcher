﻿using UnityEngine;
using System.Collections;

public class Scale : MonoBehaviour {

	// Use this for initialization
	void Start () {

		float scaleX = Screen.width;
        float scaleY = Screen.height;
	
		transform.Find("Table").transform.localScale = new Vector3 (scaleX, 1, scaleY);
        transform.Find("Camera").GetComponent<Camera>().orthographicSize = (scaleX * 9)-scaleY/16;

		if (!Application.systemLanguage.Equals (SystemLanguage.Polish)) {
			GameObject[] polski = GameObject.FindGameObjectsWithTag ("Polski");

			Util.Objects.setText (polski, false);

			GameObject[] english = GameObject.FindGameObjectsWithTag ("English");

			Util.Objects.setText (english, true);
		} else {
		
			GameObject[] polski = GameObject.FindGameObjectsWithTag ("Polski");

			Util.Objects.setText (polski, true);

			GameObject[] english = GameObject.FindGameObjectsWithTag ("English");

			Util.Objects.setText (english, false);
		}
	}
	

}
