﻿using UnityEngine;
using System.Collections;

using TouchScript.Hit;
using TouchScript.Gestures;

public class Close_app : MonoBehaviour {
	private TapGesture gesture;

	private void OnEnable()
	{

		gesture = GetComponent<TapGesture>();
		gesture.Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		gesture = GetComponent<TapGesture>();
		gesture.Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, System.EventArgs e)
	{
		Application.Quit ();
	}
}
