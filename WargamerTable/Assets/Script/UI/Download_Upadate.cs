﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using TouchScript.Hit;
using UnityEngine;

public class Download_Upadate : MonoBehaviour {

    private TapGesture gesture;
    
    private void OnEnable()
    {

        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }

  

    private void tappedHandler(object sender, System.EventArgs e)
    {
        var gesture = sender as TapGesture;
        TouchHit hit;
        gesture.GetTargetHitResult(out hit);
        Debug.Log("Send message to engine");
        GameObject.Find("System_Engine").BroadcastMessage("Download");
       
    }
}
