﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Sync_Rotation : NetworkBehaviour
{

    private Transform myTransform;

    // Object rotation vars
    [SyncVar]
    private Quaternion syncObjectRotation;
    public float rotationLerpRate = 15;

    private Quaternion lastRot;
    private float threshold = 5f;
    private Control_Ovner ovner;


    private void Start()
    {
        myTransform = GetComponent<Transform>();
        ovner = GetComponent<Control_Ovner>();

    }

    private void FixedUpdate()
    {
        LerpRotation();
        TransmitRotation();
       
    }

    private void LerpRotation()
    {

        if (!ovner.ControllerIp.Equals(""))
        {
            myTransform.rotation = Quaternion.Lerp(myTransform.rotation, syncObjectRotation, Time.deltaTime * rotationLerpRate);
        }

    }

    [Command]
    private void Cmd_ProvideRotationToServer(Quaternion objectRotation)
    {
        syncObjectRotation = objectRotation;
        RpcTransmitRotation(objectRotation);
    }

    private void TransmitRotation() {
       
            if (Quaternion.Angle(myTransform.rotation, lastRot) > threshold)
            {
                if (!isServer)
                {
                    Cmd_ProvideRotationToServer(myTransform.rotation);
                    lastRot = myTransform.rotation;

                   
                }
                else
                {
                    RpcTransmitRotation(myTransform.rotation);
                    lastRot = myTransform.rotation;
                }
                
            }
        
    }

    [ClientRpc]
    private void RpcTransmitRotation(Quaternion objectRotation)
    { // Send rotation to server

        syncObjectRotation = objectRotation;
    }
    

}