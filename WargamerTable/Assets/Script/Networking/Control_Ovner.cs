﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Control_Ovner : NetworkBehaviour {


    [SyncVar(hook = "OnVarSynced")]//this needs to be above the variable you want to sync. (you can name the "Hook" everything you want, but be aware that this is the name of a function that is called on the clients when the SyncVar changes on the server)
    public string ControllerIp; //if this changes on the server a "message" will be send to the clients automaticaly. 
                        // (if you change this on a client it will only change on THAT client, it will not be send anywhere).

    //the "Hook" function is executed on the clients when a change of the SyncVar is recieved.
    //the actual value of the SyncVar on the Clients is not changed when a change mesage is recieved from the server.

    //it is more like a trigger with a "message/variable" attached to it, 
    //it will let clients know "varToSync" has changed on the server and tell them the new value it now has on the server.

    //the string "syncedVar" is the new value of "varToSync" that it now has on the server.
    public void OnVarSynced(string syncedVar)//this is the "Hook" function that is called on clients when the Syncvar changes on the server.
    {
        //you can either use the new variable of "syncedVar" in the "Hook" function immediately or,
        //if you want the "varToSync" to keep the same value as it is on the server (so you can use it later on in the update funtion or somewhere else), 
        //you will have to change it on the clients in the "Hook" function right here.
        ControllerIp = syncedVar;

        //if you want to do something on the Local player only (the one that you are controlling)
        if (isLocalPlayer)
        {

        }

        //if you want to do something on the Remote players only (the copys of you on the other Clients)
        if (!isLocalPlayer)
        {

        }
    }

    [ClientRpc]
    public void RpcSetOvner(string arg) {
        ControllerIp=arg;
    }

    [Command]
    public void CmdSetOvner(string arg) {
        ControllerIp = arg; 
        RpcSetOvner(arg);
    }


    public void SetOvner(string arg) {
        if (isServer)
            RpcSetOvner(arg);
        else
            CmdSetOvner(arg);

        ControllerIp = arg;
       
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
