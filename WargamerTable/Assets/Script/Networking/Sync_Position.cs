﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Sync_Position : NetworkBehaviour
{



    [SerializeField]
    float lerpRate = 15;
    [SyncVar(hook = "OnVarSynced")]
    private Vector3 syncPos;

    private Vector3 lastPos;
    private float threshold = 0.5f;
    private Control_Ovner ovner;
    private Transform myTransform;


    void Start()
    {
        ovner = GetComponent<Control_Ovner>();
     
        //  myTransform = GetComponent<Transform>();
    }


    void Update()
    {
        TransmitPosition();
       // LerpPosition();
    }

    void LerpPosition()
    {
             transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * lerpRate);

    }

   
    void TransmitPosition()
    {
       
            if (Vector3.Distance(transform.position, lastPos) > threshold)
            {
                SetOvner(transform.position);
                 lastPos = transform.position;
              
            }
       
    }

    [ClientRpc]
    public void RpcSetOvner(Vector3 arg)
    {
        syncPos= arg;
        LerpPosition();
    }

    [Command]
    public void CmdSetOvner(Vector3 arg)
    {
        syncPos = arg;
        LerpPosition();
        RpcSetOvner(arg);
    }


    public void SetOvner(Vector3 arg)
    {
      

        if (isServer)
            RpcSetOvner(arg);
        else
            CmdSetOvner(arg);

        syncPos = arg;
     
    }


    public void OnVarSynced(Vector3 syncedVar)//this is the "Hook" function that is called on clients when the Syncvar changes on the server.
    {
        //you can either use the new variable of "syncedVar" in the "Hook" function immediately or,
        //if you want the "varToSync" to keep the same value as it is on the server (so you can use it later on in the update funtion or somewhere else), 
        //you will have to change it on the clients in the "Hook" function right here.
        syncPos = syncedVar;


        LerpPosition();
        //if you want to do something on the Local player only (the one that you are controlling)
        if (isLocalPlayer)
        {

        }

        //if you want to do something on the Remote players only (the copys of you on the other Clients)
        if (!isLocalPlayer)
        {

        }
    }

   
}