﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNetwork : MonoBehaviour {

   public  DownloadUpdate dow;
   public  GameObject begin;
    public SimpleHealthBar Progress;
    public Toogle_Screen togle;

    IEnumerator checkInternetConnection(Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }


    }

    IEnumerator Example()
    {

        for (int i = 0; i < 50; i++)
        {
            yield return new WaitForSeconds(0.1f);
             Progress.UpdateBar(i, 50);

        }

        begin.SetActive(false);
       togle.Toggle_one_screen();
       
       
    }
    void Start()
    {
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (isConnected)
            {
                dow.Download();
            }
            else {
                StartCoroutine(Example());

            }

        }));
    }
}
