﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Hit;

public class Toggle_two_Screen : MonoBehaviour {

    private TapGesture gesture;

   

    private void OnEnable()
    {
       
        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;

    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }


    private void tappedHandler(object sender, System.EventArgs e)
    {
        var gesture = sender as TapGesture;
        TouchHit hit;
        gesture.GetTargetHitResult(out hit);

        GameObject.FindGameObjectWithTag("Engine").SendMessage("Toggle_two_screen");
    }
}
