﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HTTP_Plugin : MonoBehaviour
{
   
    static string klasa = "httpdplugin.tidersoft.org.library.Http_Plugin";

    void Start()
    {
        HTTP_Plugin.StartSerwer();
     //   GetComponent<DownloadUpdate>().Download();
    }
    /// <summary>
    /// getIP shows local ip of serwer that can by connected.
    /// </summary>
    /// <returns>returning raw ipv4 with port</returns>
    public static string getIP() {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return  pluginClass.CallStatic<string>("getLocalIpAddress") + ":6060";

    }
    /// <summary>
    ///  StartSerwer is call to run serwer thread.
    /// </summary>
    public static void StartSerwer() {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        Debug.Log(pluginClass.CallStatic<string>("getLocalIpAddress"));

        Debug.Log(pluginClass.CallStatic<string>("StartServer"));
    }

    /// <summary>
    /// Function setUpdateSerwer is use for setting WWW adres for serwer that updateing files across the internet. 
    /// param serwer for example equals "http://example.com"
    /// </summary>
    /// <param name="serwer">is a adress for www serwer to update data</param>
    public static void setUpdateSerwer(string serwer) {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("setWWW",serwer);

    }
    /// <summary>
    /// Function serDirPath is use for setting directory on android external storage where is downloading files and is taking for the server.
    /// param path for example "MyGame"
    /// </summary>
    /// <param name="path">is a path to folder in android storage</param>
    public static void setDirPath(string path)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("setPath", path);
    }

    /// <summary>
    /// Function getDB is use for getting all records from sql database. 
    /// </summary>
    /// <param name="db">is a name of database.</param>
    /// <returns>returning JSON object</returns>
    public static string getDB(string db) {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
     return pluginClass.CallStatic<string>("getDB",db) ;

    }

    /// <summary>
    /// Function getDBValueByName is use for getting value from record in sql database by name.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="name">is a name of a record</param>
    /// <returns>returning JSON object</returns>
    public static string getDBValueByName(string db,string name)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getDBValueByName", db,name);

    }

    /// <summary>
    /// Function getDBValueById is use for getting value from record in sql database by id.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="id">is a id of record</param>
    /// <returns>returning JSON object</returns>
    public static string getDBValueById(string db, int id)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getDBValueById", db, id);

    }

    /// <summary>
    /// Function putOnDB is use for inserts record into sql database.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="name">is a name of record</param>
    /// <param name="value">is a value to insert to record</param>
    public static void putOnDB(string db,string name,string value)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("putOnDB", db,name,value);

    }
    /// <summary>
    /// Function deleteOnDB is use for deleting record from sql database by id.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="id">is a id of the record</param>
    public static void deleteOnDB(string db, int id)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("deleteOnDB", db, id);

    }

    /// <summary>
    /// Function updateOnDB is use for updating record in sql database by id.
    /// </summary>
    /// <param name="db">is a name of database</param>
    /// <param name="id">is a id of record</param>
    /// <param name="value">is a value to update</param>
    public static void updateOnDB(string db, int id,string value)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("updateOnDB", db, id,value);

    }
    /// <summary>
    /// Function UpdateFiles is use for downloading files from the www update server.
    /// </summary>
    /// <returns>Debug log</returns>
    public static string UpdateFiles(string version)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
       return pluginClass.CallStatic<string>("Update",version);

    }

    /// <summary>
    /// Function UpdateDevelop is use for downloading files from the www update server (but not public files).
    /// </summary>
    /// <returns>Debug log</returns>
    public static string UpdateDevelop()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("Develop");

    }

    /// <summary>
    /// Function getFiles is use for listing files in directory whit path.
    /// </summary>
    /// <param name="path">path for listing</param>
    /// <returns>returning JSON object</returns>
    public static string getFiles(string path)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getFiles",path);

    }

    /// <summary>
    /// Function getPlayers shows logged players to the server.
    /// </summary>
    /// <returns>returns JSON object</returns>
    public static string getPlayers()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getPlayers");

    }


    /// <summary>
    /// Function uset to send message to player in internet browser
    /// </summary>
    /// <param name="player">player name</param>
    /// <param name="msg">message to send</param>
    public static void sendBrowserMessageToPlayer(string player,string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("sendBrowserMessageToPlayer",player,msg);

    }

   
    /// <summary>
    /// Function uset to send message to all browser player.
    /// </summary>
    /// <param name="msg">message o send</param>
    public static void sendMessageToAllBrowsers(string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("sendMessageToAllBrowsers", msg);

    }

    /// <summary>
    /// Function use to debug code. Its sending to logcat logs.
    /// </summary>
    /// <param name="msg">debug message</param>
    public static void Log(string msg)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("Debug", msg);

    }

 

   /// <summary>
   /// Function that generate uniq random id using to taging or naming objects.
   /// </summary>
   /// <returns>String uniq random id</returns>
    public static string  RandomID()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("RandomID");

    }


    /// <summary>
    /// Function that gets actual WiFi network name thats connect to Android device
    /// </summary>
    /// <returns>Network name</returns>
    public static string getWifiNetwork()
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getWiFiNetwork");

    }


    /// <summary>
    /// Function that set local value to the server that can be use in brouser and unity activity.
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <param name="var">Value to store</param>
    public static void SetValue(string name,string var)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("setValue", name,var);

    }


    /// <summary>
    /// Function that returns loval value form the server by name.
    /// That value is temporery and be deleing when app is closed,
    /// </summary>
    /// <param name="name">Name of the value</param>
    /// <returns>Value as string</returns>
    public static string GetValue( string name)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getValue", name);

    }


    /// <summary>
    /// Function that store value to the player on server.
    /// That value is temporery and be deleing when app is closed.
    /// </summary>
    /// <param name="player">Name of the player</param>
    /// <param name="name">Mane of the value</param>
    /// <param name="var">Value to storeg</param>
    public static void SetPlayerValue(string player,string name, string var)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("setPlayerValue",player, name, var);

    }

    /// <summary>
    /// Function that gets player value by name.
    /// </summary>
    /// <param name="player">Player Name</param>
    /// <param name="name">Value name</param>
    /// <returns>Value as string</returns>
    public static string GetPlayerValue(string player,string name)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("getPlayerValue", player, name);

    }

    public static string sendGet(string url)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("sendGet",url);

    }
    public static string sendPost(string url, string param)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        return pluginClass.CallStatic<string>("sendPost", url, param);

    }


    public static void sendToBoard(string name, string message)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("sendToBoard", name,message);

    }

    public static void SetVarBoard(string bname, string name,string var)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("SetVarBoard", bname,name,var);

    }

    public static void updateTokenPos(string boardn, string name, float x,float y,float z)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("SetVarBoard", boardn, name, x,y,z);

    }

    public static void createBoard(string name,string message )
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("createBoard", name, message);

    }
    public static void SetVarToken(string bname, string tname,string name,string var)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("SetVarToken", bname,tname,name,var);

    }

    public static void DestroyBoard(string name)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("DestroyBoard", name);

    }

    public static void addTokenToBoard(string boardn,string name,float x,float y,float z)
    {
        AndroidJavaClass pluginClass = new AndroidJavaClass(klasa);
        pluginClass.CallStatic("addTokenToBoard",boardn,name,x,y,z);

    }
}
