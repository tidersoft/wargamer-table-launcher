﻿using UnityEngine;
using System.Collections;
using TouchScript.Hit;
using TouchScript.Gestures;

public class Create_Board : MonoBehaviour {

	private TapGesture gesture;
	public Transform board;
    public string system;

	private void OnEnable()
	{

		gesture = GetComponent<TapGesture>();
		gesture.Tapped += tappedHandler;
	}

	private void OnDisable()
	{
		gesture = GetComponent<TapGesture>();
		gesture.Tapped -= tappedHandler;
	}

	private void tappedHandler(object sender, System.EventArgs e)
	{
		var gesture = sender as TapGesture;
		TouchHit hit;
		gesture.GetTargetHitResult(out hit);

		Vector3 vec = new Vector3 (hit.Point.x, 0, hit.Point.z);
		var cube = Instantiate(board,vec,board.localRotation) as Transform;
        string nameb = system + "-" + HTTP_Plugin.RandomID();
        cube.name = nameb;
        HTTP_Plugin.createBoard(nameb,system);
		//cube.RotateAround (hit.Point, new Vector3 (1, 0, 0), 90);
		//cube.GetComponent<Destroy_Menu> ().PopParent = transform.root.transform;
		transform.root.GetComponent<Destroy_Menu>().destroy();

	}
}
