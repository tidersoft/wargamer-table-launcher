﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Behaviors;
using TouchScript.Gestures;
using TouchScript.Hit;
using UnityEngine;

public class Toogle_pin : MonoBehaviour {

    private TapGesture gesture;
    



    private void OnEnable()
    {
        gesture = GetComponent<TapGesture>();

        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();

        gesture.Tapped -= tappedHandler;
    }

    private void tappedHandler(object sender, System.EventArgs e)
    {
        GameObject obj = transform.root.GetComponent<Destroy_Menu>().PopParent.gameObject;
        if (obj.GetComponent<Transformer>().enabled)
        {
            obj.transform.Find("pin").gameObject.SetActive(true);
            obj.GetComponent<Transformer>().enabled = false;
            obj.GetComponent<Untouchable>().enabled = true;
        }
        else
        {
            obj.transform.Find("pin").gameObject.SetActive(false);
            obj.GetComponent<Untouchable>().enabled = false;
            obj.GetComponent<Transformer>().enabled = true;

        }
            transform.root.GetComponent<Destroy_Menu>().destroy();
    }
}
