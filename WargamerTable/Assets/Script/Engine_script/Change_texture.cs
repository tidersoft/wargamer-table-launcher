﻿using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using UnityEngine;

public class Change_texture : MonoBehaviour {

    private TapGesture gesture;
     public Material material;

    private void OnEnable()
    {

        gesture = GetComponent<TapGesture>();
        gesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        gesture = GetComponent<TapGesture>();
        gesture.Tapped -= tappedHandler;
    }

    private void tappedHandler(object sender, System.EventArgs e)
    {
        GameObject obj = transform.root.GetComponent<Destroy_Menu>().PopParent.gameObject;

        obj.GetComponent<Renderer>().material = material;
       
        transform.root.GetComponent<Destroy_Menu>().destroy();

    }
}
