﻿using System;
using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using TouchScript.Behaviors;
using TouchScript.Hit;
using UnityEngine.Networking;

public class Dice : MonoBehaviour {

	private TransformGesture gesture;
	private Transformer transformer;
	private Rigidbody rb;
	private Vector3 direct;
	public float SkaleY;

	private void OnEnable()
	{
		gesture = GetComponent<TransformGesture>();
		transformer = GetComponent<Transformer>();
		rb = GetComponent<Rigidbody>();

		transformer.enabled = false;
		rb.isKinematic = false;
		gesture.TransformStarted += transformStartedHandler;
		gesture.TransformCompleted += transformCompletedHandler; 
	}

	private void OnDisable()
	{
		gesture.TransformStarted -= transformStartedHandler;
		gesture.TransformCompleted -= transformCompletedHandler;
		}

	private void transformStartedHandler(object sender, EventArgs e)
	{
       
        rb.isKinematic = true;
		transformer.enabled = true;
		transform.position= new Vector3 (transform.position.x,transform.position.y+ (1 * SkaleY), transform.position.z);
		transform.Rotate(new Vector3(UnityEngine.Random.Range(-165.0f,165.0f),UnityEngine.Random.Range(-165.0f,165.0f),UnityEngine.Random.Range(-165.0f,165.0f)));
	}



	private void transformCompletedHandler(object sender, EventArgs e)
	{
     

        TouchHit hit;
		gesture.GetTargetHitResult(out hit);

		transformer.enabled = false;
		rb.isKinematic = false;
		rb.WakeUp();
		rb.AddForceAtPosition(gesture.DeltaPosition*750,hit.Point);

	}
}
