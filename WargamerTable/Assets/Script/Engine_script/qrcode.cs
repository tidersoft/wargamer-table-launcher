﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class qrcode : MonoBehaviour {

    public Transform trans;
    public int scale = 500;
    void Start()
    {
        string code = "http://"+HTTP_Plugin.getIP();
        QrCode(trans, code, scale);
    }


    public IEnumerator generate(Transform trans,string code, int scale) {
        string url = string.Format("http://chart.apis.google.com/chart?cht=qr&chs={1}x{2}&chl={0}", code, scale, scale);

        using (WWW www = new WWW(url))
        {
            yield return www;
            Renderer renderer = trans.GetComponent<Renderer>();
            renderer.material.mainTexture = www.texture;
        }
    }

    public void QrCode(Transform trans,string code, int scale) {
        StartCoroutine(generate(trans,code, scale));
    }

   
}
