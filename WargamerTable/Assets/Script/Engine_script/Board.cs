﻿using System;
using UnityEngine;
using TouchScript.Gestures;
using System.Collections;
using Util;
using System.Collections.Generic;

public class Board : MonoBehaviour
{

    public Dictionary<string,Var> vars;

    public void Start()
    {
        vars = new Dictionary<string, Var>();
    }


    void SetVar(string name,string var) {
        Var vare = new Var();
        vare.name = name;
        vare.var = var;

        if (vars.ContainsKey(name))
        {
            vars.Remove(name);
        }
            vars.Add(name,vare);

        SendVar newvar = new SendVar();

        newvar.name = transform.name;
        newvar.var = vare;

        Command cmd = new Command();
        cmd.cmd = "SetVarBoard";
        cmd.msg = JsonUtility.ToJson(newvar);

        HTTP_Plugin.sendToBoard(transform.name,JsonUtility.ToJson(cmd));
        HTTP_Plugin.SetVarBoard(transform.name, name, var);
        
    }

    void SetVarToken(string tname, string name, string var) {
        Transform tra=transform.Find("Container").Find(tname);
        tra.GetComponent<Token>().SetVar(transform.name,name, var);

        HTTP_Plugin.SetVarToken(transform.name, tname, name, var);

    }


     IEnumerator generate( string name)
    {
        string url = string.Format("http://127.0.0.1:6060/{0}", name);

        using (WWW www = new WWW(url))
        {
            yield return www;
            Renderer renderer = GetComponent<Renderer>();
            renderer.material.mainTexture = www.texture;

            Command cmd = new Command();
            cmd.cmd = "SetTextureBoard";
            cmd.msg = name;

            HTTP_Plugin.sendToBoard(transform.name, JsonUtility.ToJson(cmd));
        }
    }


    void SpawnToken(string msg) {

        SpawnPoint obj = JsonUtility.FromJson<SpawnPoint>(msg);
        Vector3 vec = new Vector3(obj.point.x, obj.point.y, obj.point.z);
        Debug.Log(obj.Object);
        GameObject board = Resources.Load("Tokens/Blank") as GameObject;
        if (board == null) Debug.Log("Brak Object");

        var cube = Instantiate(board.transform, vec, board.transform.localRotation) as Transform;
        cube.name = "Token-"+HTTP_Plugin.RandomID();
        obj.name = cube.name;
        cube.GetComponent<Token>().system = obj.system;
        cube.GetComponent<Token>().Object = obj.Object;
        //cube.GetComponent<Token>().nowa = this;
        cube.parent = transform;
        cube.GetComponent<Token>().parent = transform.name;
        cube.GetComponent<Token>().addToBoard();
        cube.localPosition = vec;
        cube.GetComponent<OBJ>().Run(obj);
        SpawnPoint trans = new SpawnPoint();
        trans.name = obj.name;
        trans.point = cube.localPosition;
        trans.Object = cube.GetComponent<Token>().Object;
        trans.system = cube.GetComponent<Token>().system;

        Command cmd = new Command();
        cmd.cmd = "Spawn";
        cmd.msg = JsonUtility.ToJson(trans);
        // HTTP_Plugin.Log("Trans " + trans.name + ":" + trans.vec.ToString());
        HTTP_Plugin.updateTokenPos(transform.name, trans.name, trans.point.x, trans.point.y, trans.point.z);
        HTTP_Plugin.sendToBoard(transform.name, JsonUtility.ToJson(cmd));

    }



    public void setTexture(string name) {
        StartCoroutine(generate(name));


    }


}
