﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomId : MonoBehaviour {

    public string Id;

    public string ObjectType;

    public string ParentId;

    string url ="http://localhost:6060/setId?";

    // Use this for initialization
    IEnumerator Start () {
        AndroidJavaClass pluginclass = new AndroidJavaClass("httpdplugin.tidersoft.org.library.My_Plugin");
       Id=pluginclass.CallStatic<string>("RandomID");
        ParentId = transform.parent.GetComponent<RandomId>().Id;

        url += "Id=" + Id + "&Typ=" + ObjectType + "&Parent=" + ParentId;
        var r = new HTTP.Request("GET", url);
        yield return r.Send();

        if (r.exception == null)
        {
            Debug.Log("HTTPS RESPONSE: " + r.response.status);
        }
        else
        {
            Debug.LogException(r.exception);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
