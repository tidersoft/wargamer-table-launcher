﻿using System;
using UnityEngine;
using TouchScript.Gestures;
using TouchScript.Behaviors;
using Util;
using System.Collections.Generic;

public class Checker : MonoBehaviour
    {
        private TransformGesture gesture;
        private Transformer transformer;
        private Rigidbody rb;
		public float SkaleY;
   


   


    void Start() {
    }


	   private void OnEnable()
        {
            gesture = GetComponent<TransformGesture>();
            transformer = GetComponent<Transformer>();
            rb = GetComponent<Rigidbody>();

            transformer.enabled = false;
            rb.isKinematic = false;
            gesture.TransformStarted += transformStartedHandler;
            gesture.TransformCompleted += transformCompletedHandler;   
		}

        private void OnDisable()
        {
            gesture.TransformStarted -= transformStartedHandler;
            gesture.TransformCompleted -= transformCompletedHandler;
        }

        private void transformStartedHandler(object sender, EventArgs e)
        {
            rb.isKinematic = true;
            transformer.enabled = true;
		transform.localRotation = Quaternion.Euler (Vector3.zero);
			transform.Translate (new Vector3(0,2*SkaleY,0));
		}

        private void transformCompletedHandler(object sender, EventArgs e)
        {
            transformer.enabled = false;
            rb.isKinematic = false;
            rb.WakeUp();
        Command cmd = new Command();
        cmd.cmd = "Trans";
        Trans trans = new Trans();
        trans.name = transform.name;
        trans.vec = transform.localPosition;
        cmd.msg = JsonUtility.ToJson(trans);
        HTTP_Plugin.updateTokenPos(transform.parent.parent.name,trans.name,trans.vec.x,trans.vec.y,trans.vec.z);
        HTTP_Plugin.sendToBoard(transform.parent.parent.name, JsonUtility.ToJson(cmd));
		//	transform.Translate(new Vector3(0,-2*SkaleY,0));
        }
    }
