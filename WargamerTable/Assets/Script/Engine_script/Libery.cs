﻿using UnityEngine;
using System.Collections;
using System;

namespace Util{

    [Serializable]
    public class Command
    {

        public string cmd;
        public string msg;
    }

    [Serializable]
    public class Trans
    {
        public Vector3 vec;
        public string name;
    }

    [Serializable]
    public class Var {
        public string name;
        public string var;
    }

    [Serializable]
    public class SendVar {
        public string name;
        public Var var;
    }

    


    public class Http : MonoBehaviour {

        public static IEnumerator HttpRequest(string url) {
            var r = new HTTP.Request("GET", url);
            yield return r.Send();

            if (r.exception == null)
            {
                Debug.Log("HTTPS RESPONSE: " + r.response.status);
            }
            else
            {
                Debug.LogException(r.exception);
            }
        }

       
    }

  
   
    public class Tokens{



	public static Transform getTokenRoot(Transform trans){
	
		if (trans.tag.Equals ("Token"))
			return trans;

		return getTokenRoot(trans.parent.transform);
	
	}


}

public class Objects{

		public static void setText(GameObject[] obj,bool en){

			for (int i = 0; i < obj.Length; i++) {
			
				obj [i].SetActive(en);
			
			}

	}

        public static Transform getParentOb(Transform Object)
        {
            if (Object.GetComponent<Destroy_Menu>() != null)
                if (Object.GetComponent<Destroy_Menu>().PopParent != null)
                {
                    return getParentOb(Object.GetComponent<Destroy_Menu>().PopParent);
                }

            return Object;
        }

    }

}