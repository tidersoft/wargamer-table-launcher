﻿using UnityEngine;
using System;
using Util;

public class Change_board : MonoBehaviour {
    private Rigidbody rb;


    void OnCollisionEnter(Collision col) {


        if (col.gameObject.transform.tag.Equals("Token") || col.gameObject.transform.tag.Equals("TokenElement")) {
            GameObject obj = Util.Tokens.getTokenRoot(col.gameObject.transform).gameObject;
            rb = obj.GetComponent<Rigidbody>();

            rb.isKinematic = true;

            if (obj.tag.Equals("Token")) {
                obj.transform.SetParent(transform.root.transform.Find("Container"));
            }
        }
    }

    void GetPositionsToken() {

        Transform conte = transform.Find("Container");
        for (int i = 0; i < conte.childCount; i++) {
            SpawnPoint trans = new SpawnPoint();
            trans.name = conte.GetChild(i).name;
            trans.point = conte.GetChild(i).localPosition;
            trans.Object = conte.GetChild(i).GetComponent<Token>().Object;
            trans.system = conte.GetChild(i).GetComponent<Token>().system;

            Command cmd = new Command();
            cmd.cmd = "Spawn";
            cmd.msg = JsonUtility.ToJson(trans);
            // HTTP_Plugin.Log("Trans " + trans.name + ":" + trans.vec.ToString());
            HTTP_Plugin.updateTokenPos(transform.name, trans.name, trans.point.x, trans.point.y, trans.point.z);
            HTTP_Plugin.sendToBoard(transform.name, JsonUtility.ToJson(cmd));

        }
    }


    void SetPositionToken(string mesage)
    {
       // HTTP_Plugin.Log(mesage);
        Trans trans = JsonUtility.FromJson<Trans>(mesage);
        Command cmd = new Command();
        cmd.cmd = "Trans";
        cmd.msg = mesage;
        // HTTP_Plugin.Log("Trans " + trans.name + ":" + trans.vec.ToString());
        HTTP_Plugin.updateTokenPos(transform.name, trans.name, trans.vec.x, trans.vec.y, trans.vec.z);
        HTTP_Plugin.sendToBoard(transform.name, JsonUtility.ToJson(cmd));

        transform.Find("Container").transform.Find(trans.name).localPosition = trans.vec;
    }
}
